$(document).ready(function(){
    $("input").keyup(function(){ 
        
        var key = $(this).val();

        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?" + "q=" + key, 
            success: function(result) {
                $(".container").empty();
                for(i = 0; i < result.items.length; i++) {
                    $(".container").append(
                        "<div class='book'>" +
                        "<img src=" + result.items[i].volumeInfo.imageLinks.thumbnail + ">" +
                        "<h4>" + result.items[i].volumeInfo.title + "</h4>" +
                        "<h6>(" + result.items[i].volumeInfo.authors[0] + ")</h6>" +
                        "</div>"
                    );
                }       
            }
         });
    });
});